//console.log("Test connection");

// JAVASCRIPT SYNCHRONOUS VS ASYNCHRONOUS

// SYNCHRONOUS----------------------------------------------------------------
// Javascript is synchronous: it can execute 1 statement/line of code at a time

console.log("Hello world!");
//conosle.log("Hello again");
console.log("Goodbye");

console.log("Hello world!");
// for(let i = 0; i <= 1500; i++){
// 	console.log(i);
// }

console.log("Hello Again!");


// Getting All Post ------------------------------------------------------------

// fetch() -fetch request
// fetch API allows programmers to asynchronously request for a resource (data)

fetch("https://jsonplaceholder.typicode.com/posts")

// Use "json" from response object to convert data into JSON format
.then((response) => response.json())
.then((json) => console.log(json));

// The "async" and "await" keywords are approach to achieve asynchronous
// Used in functions to indicate which portions of the code should be awaited for

async function fetchData(){

	// wait the fetch method to be complete then store value in result variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	// result returned "response"
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	// convert data to JSON Format
	let json = await result.json();
	console.log(json);
}

fetchData();







// Getting a specific post (GET) -----------------------------------------------------
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response) => response.json())
.then((json) => console.log(json));



// Creating a post (POST) ---------------------------------------------------------------
/*
fetch("url", {options})
.then((response) => {})//promise 1
.then((response) => {})//promise 2
*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then((response) => response.json())//convert response to json format
.then((json) => console.log(json));//display in console


// Updating a post (PUT) change all property------------------------------------------
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		id:1,
		title: "Updated Post",
		body: "Hello Again!",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// Updating a post (PATCH) change specific property-------------------------------
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post",
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Deleting a post (DELETE) ---------------------------------------------------------------
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});

// Filtering a post (DELETE) -----------------------------------------------------------

// Individual Parameter
fetch("https://jsonplaceholder.typicode.com/posts?userId=2")
.then((response) => response.json())
.then((json) => console.log(json));

// get comments from post
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response) => response.json())
.then((json) => console.log(json));

// Multiple Parameter
//url?paramA=valueA&paramB=valueB
